package main

import (
    "net/http"
    "os"
    "log"
    "time"

    "gitLabAPI/Functions"
    "gitLabAPI/Variables"

	  "golang.org/x/net/context"
    "google.golang.org/api/option"

	  firebase "firebase.google.com/go"
)


//------------------------------------------------------------------------------
// Main function ---------------------------------------------------------------
func main() {

  // Starts time ---------------------------------------------------------------
  Variables.StartTime = time.Now().UTC()


  // Conecting to the firebase database ----------------------------------------
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile(Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Sets the localhost port to 8080 -------------------------------------------
  port := os.Getenv("PORT")
      if port == "" {
      port = "8080"
  }


  // Functions retriving the url -----------------------------------------------
  //  and starting the functions for the endpoints
  http.HandleFunc("/", Functions.BadRequest)
  http.HandleFunc("/repocheck/v1/commits", Functions.Commits)
  http.HandleFunc("/repocheck/v1/languages", Functions.Language)
  http.HandleFunc("/repocheck/v1/issues", Functions.Issues)
  http.HandleFunc("/repocheck/v1/status", Functions.Status)
  http.HandleFunc("/repocheck/v1/webhooks/", Functions.Webhooks)
  // Lets the user reenter input again and again
  http.ListenAndServe(":" + port, nil)
}
