package Variables

import(
  "time"
  "cloud.google.com/go/firestore"
	"golang.org/x/net/context"
)



// StartTime global declatation -----------------------------------------------
var StartTime time.Time



// Int constansts --------------------------------------------------------------

// MaxRepo represents the amout of project ids the app need to go though
const MaxRepo int = 60
 // Limit default
const Limit int = 5



// Version ---------------------------------------------------------------------
const Version string = "v1"



// URL constants ---------------------------------------------------------------
// const Token string = "6z8oKCNRogYwDMxd9pCy"

// DataBaseURL to get database url
const DataBaseURL string = "https://firebase.google.com/"


// GitURLPro to get git url with projects
const GitURLPro string = "https://git.gvk.idi.ntnu.no/api/v4/projects/"
// GitURL to get git url without projects
const GitURL string = "https://git.gvk.idi.ntnu.no/api/v4/"


// SearchURL looks for projects with given name, so ID can be found
const SearchURL string = "search?scope=projects&search="


// ComURL to find every commit by everyone who contributed a commit
const ComURL string = "/repository/contributors"
// LangURL to find every lang used in every project
const LangURL string = "/languages"
// IssURL to find every issue
const IssURL string = "/issues"


// And to add an & before access tolken
const And string = "&"
// Quest to add an ? before access tolken
const Quest string = "?"
// Auth to add access tolken
const Auth string = "access_token="



// Message constants -----------------------------------------------------------

// NotInUse to explain that function is not implemented
const NotInUse string = " is not used in this application"
// NoID to explain that ID does not exist
const NoID string = "ID does not exist"
// NotLimit to explain limit is not valid
const NotLimit string = "Limit is not valid (not an int)"
// WrongLangBody to explain that body is wrong
const WrongLangBody string = "Body is wrong. Try:\n\n[ \"project1\", \"project2\", ... ]"
// EmptyIssBody to explain that body is empty
const EmptyIssBody string = "Body is empty. Try:\n\n{\n\t\"project\": \"project name\"\n}"
// EmptyPostBody to explain that body is empty
const EmptyPostBody string = "Body is empty. Try:\n\n{\n\t\"event\": \"(commits|languages|issues|status)\",\n\t\"url\": \"webhook url\"\n}"

// BadRequest to tell how to use app
const BadRequest string = "Try insted:"
// BadRequestCom to tell how to use commits
const BadRequestCom string = "\n\n\t/repocheck/v1/commits\n\t/repocheck/v1/commits?limit=[0-9]+\n\t/repocheck/v1/commits?auth=<access-token>\n\t/repocheck/v1/commits?limit=[0-9]+&auth=<access-token>"
// BadRequestLang to tell how to use languages
const BadRequestLang string = "\n\n\t/repocheck/v1/languages\n\t/repocheck/v1/languages?limit=[0-9]+\n\t/repocheck/v1/languages?auth=<access-token>\n\t/repocheck/v1/languages?limit=[0-9]+&auth=<access-token>"
// BadRequestIss to tell how to use issues
const BadRequestIss string = "\n\n\t/repocheck/v1/issues\n\t/repocheck/v1/issues??type=(users|labels)\n\t/repocheck/v1/issues?auth=<access-token>\n\t/repocheck/v1/issues??type=(users|labels)&auth=<access-token>"
// BadRequestStat to tell how to use stats
const BadRequestStat string = "\n\n\t/repocheck/v1/status"
// BadRequestWH to tell how to use webhooks
const BadRequestWH string = "\n\n\t/repocheck/v1/webhooks\n\t/repocheck/v1/webhooks/ID"
// WHDeleted to tell you webhooks deleted
const WHDeleted string = "Webhook deleted"



// FirebaseJSON database constant ----------------------------------------------
const FirebaseJSON string = "./gitlabapi-2f7b9-firebase-adminsdk-dsh54-1e671a3fd6.json"



// Firebase global variables ---------------------------------------------------

// Ctx to connect to database
var Ctx context.Context
// Client to connect to client
var Client *firestore.Client
