package Variables

import(
    "time"
)


// Commits structs -------------------------------------------------------------

// ReadInCom gets every commit from every contributer
type ReadInCom struct {
  Commits int `json:"commits"`
}

// Repos gets in name and all collective commits
type Repos struct {
  Repository string `json:"path_with_namespace"`
  Commits int `json:"commits"`
}

// Commit gets in all repos, with and without authentication
type Commit struct {
  Repos []Repos `json:"repos"`
  Authentication bool `json:"auth"`
}


// Languages structs -----------------------------------------------------------

// ResponseLang reads inn every languages used
type ResponseLang struct {
  Languages map[string]interface{} `json:"-"`
}

// Lang reads in languages and count how many times it has been used
type Lang struct {
  Lang string `json:"languages"`
  Count int `json:"count"`
}

// Languages gets in all languages from requested repos, with and without authentication
type Languages struct {
  Languages []string `json:"languages"`
  Authentication bool `json:"auth"`
}


//  Issues structs -------------------------------------------------------------
//  User structs

// Author gets in all usernames form every issue author
type Author struct {
  UserName UserName `json:"author"`
}

// UserName gets in all usernames
type UserName struct {
  Username string `json:"username"`
}

// CountUser counts all users who has created issues
type CountUser struct {
  Username string `json:"username"`
  Count int `json:"count"`
}

// User gets in all users from requested repos, with and without authentication
type User struct {
  Users []CountUser `json:"users"`
  Authentication bool `json:"auth"`
}



//  Labels structs

// ReadInLab gets in all labels on a issues
type ReadInLab struct {
  ReadIN []string `json:"labels"`
}

// CountLab counts all labels on a issues
type CountLab struct {
  Label string `json:"label"`
  Count int `json:"count"`
}

// Labels gets in all labels from requested repos, with and without authentication
type Labels struct {
  Labels []CountLab `json:"lables"`
  Authentication bool `json:"auth"`
}

// ProName gets in all project names searched for
type ProName struct {
  Project string `json:"project"`
}



// ProID used in multiple funcs struct to collect project ids -----------------
type ProID struct {
  ID int `json:"id"`
}


// Status struct --------------------------------------------------------------
type Status struct {
  Git int `json:"git"`
  Database int `json:"database"`
  UpTime int `json:"uptime"`
  Version string `json:"v1"`
}


//  Webhooks structs -----------------------------------------------------------

// PostWeb to get get in what to send to database
type PostWeb struct {
  Event string `json:"event" firestore:"Event,omitempty"`
  URL string `json:"url" firestore:"URL,omitempty"`
}

// ViewWebResponse to veiw what is in the database
type ViewWebResponse struct {
  ID string `json:"id"`
  Event string `json:"event"`
  URL string `json:"url"`
  Time time.Time `json:"time"`
}

// GetWebInvocation to send to those who asked for a webhook
type GetWebInvocation struct {
  Event string `json:"event"`
  Parameters string `json:"params"`
  Time time.Time `json:"time"`
}
