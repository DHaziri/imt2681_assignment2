package Functions

import(
  "encoding/json"
  "fmt"
  "io/ioutil"
  "net/http"
  "gitLabAPI/Variables"
  "strings"
  "strconv"

)



// Issues ----------------------------------------------------------------------
func Issues(w http.ResponseWriter, r *http.Request)  {
  w.Header().Set("Content-Type", "application/json")


  // Gets the parameters from the path -----------------------------------------
  _, tempIss, err := SplitsPara(w, r)
  if err != nil {
    return
  } // Adds accsess token if recived
  WorWOTolken := ""
  WorWOTolken += tempIss


  // Checks for type and that it is correct ------------------------------------
  UoLType := strings.Split(r.URL.RawQuery, "type=")
  if len(UoLType) == 1 {
      BadRequest(w, r)
      return
  }
  aUoLType := strings.Split(UoLType[1], "&")
  if len(aUoLType) == 2 {
    if aUoLType[0] != "users" && aUoLType[0] != "labels" {
      BadRequest(w, r)
      return
    }
  } else if UoLType[1] != "users" && UoLType[1] != "labels" {
    BadRequest(w, r)
    return
  }


  // Gets a string of the parameters and sends it to invocation ----------------
  splits := strings.Split(r.URL.Path, "issues")
  Invocation(w,"issues", splits[1])


  // Checks for body -----------------------------------------------------------
  requestData, err := ioutil.ReadAll(r.Body)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  var proName Variables.ProName
  err = json.Unmarshal(requestData, &proName)
  if err != nil {
    // If body can't be found, send error
    fmt.Fprintln(w, Variables.EmptyIssBody)


  // If body can be found ------------------------------------------------------
  } else {

    // Search for and finds the id for given name ------------------------------
    var idObject []Variables.ProID
    url := Variables.GitURL
    url += Variables.SearchURL
    url += proName.Project
    url += Variables.And
    url += WorWOTolken

    // Reads in json from url, and puts it into an object
    resID, err := http.Get(url)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    iData, err := ioutil.ReadAll(resID.Body)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    json.Unmarshal(iData, &idObject)


    // If type is users --------------------------------------------------------
    if UoLType[1] == "users" || aUoLType[0] == "users" {

      // Declares the struct array that will contain all the objects -----------
      //  and starts a for loop that goes through all the project IDs
      var sortObject []Variables.CountUser
      for i := range idObject {
        // Converting the id int to string, and declare the url
        url := Variables.GitURLPro
        id := strconv.Itoa(idObject[i].ID)
        url += id
        url += Variables.IssURL
        url += Variables.Quest
        url += WorWOTolken


        // Reads in json from url, and puts it into an object
        resInfo, err := http.Get(url)
        if err != nil {
          w.WriteHeader(http.StatusInternalServerError)
          return
        }
        resData, err := ioutil.ReadAll(resInfo.Body)
        if err != nil {
          w.WriteHeader(http.StatusInternalServerError)
          return
        }
        var responseObject []Variables.Author
        json.Unmarshal(resData, &responseObject)


        // Transfers and counts all the different Users into a different struct
        found := false
        //      forloop the length of the amount of each user with an issue
        for k := range responseObject {
        //      forloop the length of the already counted users
          for j := range sortObject {
        //      if users is alredy counted at least once, add one
            if sortObject[j].Username == responseObject[k].UserName.Username {
              found = true
              sortObject[j].Count++
            }
          }
        //      if user has not been counted already, add it to the struct
          if !found {
            newSort := Variables.CountUser{Username: responseObject[k].UserName.Username, Count: 1}
            sortObject = append(sortObject, newSort)
          }
        }
      }

      // Declaring the final struct and filling it according to the limit ------
      var finalObject Variables.User
      for i := 0;  i < len(sortObject); i++ {
        finalObject.Users = append(finalObject.Users, sortObject[i])
      }
      if tempIss != "" {
        finalObject.Authentication = true
      } else {
        finalObject.Authentication = false
      }


      // Converts and writing out the bytes and users --------------------------
      issueBytes, err := json.Marshal(finalObject)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      w.Write(issueBytes)


    // If type is labels -------------------------------------------------------
    } else if UoLType[1] == "labels" || aUoLType[0] == "labels"  {
      // Declares the struct array that will contain all the objects -----------
      var sortObject []Variables.CountLab

      // Forloop the length of the amount of id's with given name
      for i := range idObject {
        // Converting the id int to string, and declare the url
        url := Variables.GitURLPro
        id := strconv.Itoa(idObject[i].ID)
        url += id
        url += Variables.IssURL
        url += Variables.Quest
        url += WorWOTolken


        // Reads in json from url, and puts it into an object ------------------
        resInfo, err := http.Get(url)
        if err != nil {
          w.WriteHeader(http.StatusInternalServerError)
          return
        }
        resData, err := ioutil.ReadAll(resInfo.Body)
        if err != nil {
          w.WriteHeader(http.StatusInternalServerError)
          return
        }
        var responseObject []Variables.ReadInLab
        json.Unmarshal(resData, &responseObject)


        // Transfers and counts all the different Users into a different struct
        //      forloop the length of the amount of each issue with a label
        for k := range responseObject {
        //      forloop the lenght of the amount of each label on a issue
          for l := range responseObject[k].ReadIN {
            found := false
        //      forloop the length of the already counted users
            for j := range sortObject {
        //      if users is alredy counted at least once, add one
              if sortObject[j].Label == responseObject[k].ReadIN[l] {
                found = true
                sortObject[j].Count++
                fmt.Println("adding one to type: ", responseObject[k].ReadIN[l])
              }
            }
        //      if user has not been counted already, add it to the struct
            if !found {
              newSort := Variables.CountLab{Label: responseObject[k].ReadIN[l], Count: 1}
              sortObject = append(sortObject, newSort)
            }
          }
        }

      }


      // Declaring the final struct and filling it according to the limit ------
      var finalObject Variables.Labels
      for i := 0;  i < len(sortObject); i++ {
        finalObject.Labels = append(finalObject.Labels, sortObject[i])
      }
      if tempIss != "" {
        finalObject.Authentication = true
      } else {
        finalObject.Authentication = false
      }


      // Converts and writing out the bytes and labels -------------------------
      issueBytes, err := json.Marshal(finalObject)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      w.Write(issueBytes)
    }
  }
  w.WriteHeader(http.StatusOK)
}
