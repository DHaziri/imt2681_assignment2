package Functions

import(
//"log"
"net/http"
"fmt"
"strings"
"gitLabAPI/Variables"
"strconv"
)



// BadRequest returns how the user is supposed to write a path -----------------
func BadRequest (w http.ResponseWriter, r *http.Request) {
  http.Error(w, "Bad Request", http.StatusBadRequest)
  fmt.Fprintf(w, Variables.BadRequest + Variables.BadRequestCom + Variables.BadRequestLang + Variables.BadRequestIss + Variables.BadRequestStat + Variables.BadRequestWH)
}



// SplitsPara splits and returns the different parameters ----------------------
func SplitsPara (w http.ResponseWriter, r *http.Request) (int, string, error) {


  // Declare the returns
  lim := Variables.Limit
  urlWorWOAuth := ""
  var err error

  // Splits the URL parameters
  limSplits := strings.Split(r.URL.RawQuery, "limit=")
  authSplits := strings.Split(r.URL.RawQuery, "auth=")


  // If both parameters are used -----------------------------------------------
  if len(limSplits) == 2 && len(authSplits) == 2 {

    // Splits auth of limit, and convert limit to an int
    limAuthSplits := strings.Split(limSplits[1], "&auth=")
    tempLim, tempErr := strconv.Atoi(limAuthSplits[0])
    if tempErr != nil{
      fmt.Fprintln(w, Variables.NotLimit)
      err = tempErr
    }
    lim = tempLim


  // If only limit is used -----------------------------------------------------
  } else if len(limSplits) == 2 {
    // Converts limit to an int
    tempLim, tempErr := strconv.Atoi(limSplits[1])
    if tempErr != nil{
      fmt.Fprintln(w, Variables.NotLimit)
      err = tempErr
    }
    lim = tempLim

  }


  // As long as auth exist regardless of limit ---------------------------------
  if len(authSplits) == 2 {

    // Creates an access token to put at the end of an url
    urlWorWOAuth = Variables.Auth
    urlWorWOAuth += authSplits[1]
  }

  return lim, urlWorWOAuth, err
}
