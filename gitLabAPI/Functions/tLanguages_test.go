package Functions

import(
  "net/http"
  "net/http/httptest"
  "testing"
  "bytes"
  "gitLabAPI/Variables"
  "log"

  "golang.org/x/net/context"
  "google.golang.org/api/option"

  firebase "firebase.google.com/go"
)


// TestLanguageBadBody tests wrong body ----------------------------------------
func TestLanguageBadBody(t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(`{"onecity"}`)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/languages", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Language)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `got Body is wrong. Try:

          [ "project1", "project2", ... ]`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestLanguageRightBody tests right body with auth ----------------------------
func TestLanguageRightBody(t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
  }
  Variables.Client, err = app.Firestore(Variables.Ctx)
  if err != nil {
    log.Print(err)
  }
  defer Variables.Client.Close()


  // Response getter
  byt := []byte(`["onecity"]`)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/languages?auth=6z8oKCNRogYwDMxd9pCy", respons)
  if err != nil {
        t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Language)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `{"languages":["JavaScript","ShaderLab","HLSL","C#","PLSQL"],"auth":true}`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestLanguageNoBody tests without with limit body ----------------------------
func TestLanguageNoBody(t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
  }
  Variables.Client, err = app.Firestore(Variables.Ctx)
  if err != nil {
    log.Print(err)
  }
  defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/languages?limit=2", respons)
    if err != nil {
        t.Fatal(err)
    }


  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Language)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `{
    "languages": [
        "Java",
        "Go"
    ],
    "auth": false
    }`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
    }
}
