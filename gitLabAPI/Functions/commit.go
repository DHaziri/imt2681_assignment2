package Functions

import (
    "encoding/json"
    "io/ioutil"
    "net/http"
    "gitLabAPI/Variables"
    "strconv"
    "sort"
    "strings"
)


// Commits ---------------------------------------------------------------------
func Commits(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")


  // Gets the parameters from the path -----------------------------------------
  lim, tempCom, err := SplitsPara(w, r)
  if err != nil {
    return
  } // Adds accsess token if recived
  WorWOTolken := ""
  WorWOTolken += tempCom


  // Gets a string of the parameters and sends it to invocation ----------------
  splits := strings.Split(r.URL.Path, "commits")
  Invocation(w, "commits", splits[1])


  // Declares the struct array that will contain all the objects ---------------
  //  and starts a for loop that goes through all the project IDs
  var responseObject []Variables.Repos
  for i := 0; i < Variables.MaxRepo; i++ {
    // Converting the id int to string, and declare the url
    id := strconv.Itoa(i)
    url := Variables.GitURLPro
    url += id
    url += Variables.Quest
    url += WorWOTolken


    // Reads in json from url, and puts it into an object
    resInfo, err := http.Get(url)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    resData, err := ioutil.ReadAll(resInfo.Body)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    var readRepoResponse Variables.Repos
    json.Unmarshal(resData, &readRepoResponse)


    // Makes sure that empty repositories are not included
    if readRepoResponse.Repository != "" {

      // Converting the id int to string, and declare the url
      url := Variables.GitURLPro
      url += id
      url += Variables.ComURL
      url += Variables.Quest
      url += WorWOTolken

      // Reads in json from url, and puts it into an object
      resInfo, err := http.Get(url)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      resData, err := ioutil.ReadAll(resInfo.Body)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      var readComResponse []Variables.ReadInCom
      json.Unmarshal(resData, &readComResponse)


      // Adds togeather all the commits from each contributer
      for k := range readComResponse {
        readRepoResponse.Commits += readComResponse[k].Commits
      }


      // Adds this single object to an array og objects
      responseObject = append(responseObject, readRepoResponse)
    }
  }


  // Sorts the repositoris according to the commits ----------------------------
  sort.Slice(responseObject, func(i, j int) bool {
    return responseObject[i].Commits > responseObject[j].Commits
  })


  // Declaring the final struct and filling it according to the limit ----------
  var finalObject Variables.Commit
  for i := 0; i < lim && i < len(responseObject); i++ {
      finalObject.Repos = append(finalObject.Repos, responseObject[i])
  }
  if tempCom != "" {
    finalObject.Authentication = true
  } else {
    finalObject.Authentication = false
  }


  // Converts and writing out the bytes and commits ----------------------------
  commitBytes, err := json.Marshal(finalObject)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  w.Write(commitBytes)
  w.WriteHeader(http.StatusOK)
}
