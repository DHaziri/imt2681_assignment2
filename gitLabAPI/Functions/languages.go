package Functions

import(
  "encoding/json"
  "io/ioutil"
  "net/http"
  "gitLabAPI/Variables"
  "strconv"
  "sort"
  "strings"
  "fmt"
)


// APIResponse gets the Apis and counts them -----------------------------------
func APIResponse (w http.ResponseWriter, sortObject []Variables.Lang, url string) ([]Variables.Lang, error) {
  // Declare the returns
  var err error


  // Gets the url for countries ------------------------------------------------
  resInfo, tempErr := http.Get(url)
  if tempErr != nil {
    w.WriteHeader(http.StatusInternalServerError)
    err = tempErr
  }
  resData, tempErr := ioutil.ReadAll(resInfo.Body)
  if tempErr != nil {
    w.WriteHeader(http.StatusInternalServerError)
    err = tempErr
  }
  var responseObject Variables.ResponseLang
  json.Unmarshal(resData, &responseObject.Languages)

  // Deletes messages so they are not counted
  delete(responseObject.Languages, "message")


  // Transfers and counts all the different languages into a different struct --
  found := false
  //     forloop the length of the amount of langs in each repository
  for k := range responseObject.Languages {
  //     forloop the length of the already counted langs
    for j := range sortObject {
  //     if lang is alredy counted at least once, add once
      if sortObject[j].Lang == k {
            found = true
            sortObject[j].Count++
      }
    }
  //     if  lang is not been counted already, add it to the struct
    if !found {
      newLang := Variables.Lang{Lang: k, Count: 1}
      sortObject = append(sortObject, newLang)
    }
  }

  return sortObject, err
}



// Language gets and returns data on requested languages -----------------------
func Language(w http.ResponseWriter, r *http.Request){
    w.Header().Set("Content-Type", "application/json")


  // Gets the parameters from the path -----------------------------------------
  lim, tempLang, err := SplitsPara(w, r)
  if err != nil {
    return
  } // Adds accsess token if recived
  WorWOTolken := ""
  WorWOTolken += tempLang


  // Gets a string of the parameters and sends it to invocation ----------------
  splits := strings.Split(r.URL.Path, "languages")
  Invocation(w, "languages", splits[1])


  // Declares the struct array that will contain all the objects ---------------
  var sortObject []Variables.Lang
  // Tries to get a body
  requestData, err := ioutil.ReadAll(r.Body)
  if err != nil {
     w.WriteHeader(http.StatusInternalServerError)
     return
  }

  var proName []string
  nErr := json.Unmarshal(requestData, &proName)


  // If body exist but is not right --------------------------------------------
  if nErr != nil && nErr.Error() != "unexpected end of JSON input"{
    // If right body can't be found, send error
    fmt.Fprintln(w, Variables.WrongLangBody)
    return


  // If body is empty, go through all repositorie ids --------------------------
  } else if nErr != nil {
    for i := 1; i <= Variables.MaxRepo; i++ {
        // Converting the id int to string, and declare the url
      id := strconv.Itoa(i)
      url := Variables.GitURLPro
      url += id
      url += Variables.LangURL
      url += Variables.Quest
      url += WorWOTolken


      // Gets the Api and counts them
      sortObject, err = APIResponse(w, sortObject, url)
      if err != nil {
        return
      }
    }


  // If body exist and is right ------------------------------------------------
  } else if err == nil {

    // Search for and finds the ids for the given names ------------------------
    var idObject []Variables.ProID
    for i := 0; i < len(proName); i++ {
      url := Variables.GitURL
      url += Variables.SearchURL
      url += proName[i]
      url += Variables.And
      url += WorWOTolken

      // Reads in json from url, and puts it into an object
      resID, err := http.Get(url)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      iData, err := ioutil.ReadAll(resID.Body)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      json.Unmarshal(iData, &idObject)
    }


    //  Starts a for loop that goes through all the project IDs ----------------
    for i := 0; i < len(idObject); i++ {
      // Converting the id int to string, and declare the url
      id := strconv.Itoa(idObject[i].ID)
      url := Variables.GitURLPro
      url += id
      url += Variables.LangURL
      url += Variables.Quest
      url += WorWOTolken

      // Gets the Apis and counts them
      sortObject, err = APIResponse(w, sortObject, url)
      if err != nil {
        return
      }
    }

  }


  // Sorts the Languages based on count ----------------------------------------
  sort.Slice(sortObject[:], func(i, j int) bool {
    return sortObject[i].Count > sortObject[j].Count
  })


  // Declares and transfers the langs to desired object ------------------------
  var finalObject Variables.Languages
  for i := 0; i < lim && i < len(sortObject); i++ {
    finalObject.Languages = append(finalObject.Languages, sortObject[i].Lang)
  }
  if tempLang != "" {
    finalObject.Authentication = true
  } else {
    finalObject.Authentication = false
  }


  // Converts and writing out the bytes and lang -------------------------------
  langBytes, err := json.Marshal(finalObject)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  w.Write(langBytes)
  w.WriteHeader(http.StatusOK)
}
