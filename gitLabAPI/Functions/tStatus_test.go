package Functions

import(
  "net/http"
  "net/http/httptest"
  "testing"
  "bytes"
)


// TestStatus tests status -----------------------------------------------------
func TestStaus(t *testing.T) {
  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/status", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Status)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `{
    "git": 200,
    "database": 200,
    "uptime": 1211,
    "v1": "v1"
    }`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}
