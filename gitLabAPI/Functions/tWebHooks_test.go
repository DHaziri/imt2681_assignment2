package Functions

import(
  "net/http"
  "net/http/httptest"
  "testing"
  "bytes"
  "gitLabAPI/Variables"
  "log"

  "golang.org/x/net/context"
  "google.golang.org/api/option"

  firebase "firebase.google.com/go"
)


// TestWHNotUsed tests not used message ----------------------------------------
func TestWHNotUsed(t *testing.T) {
  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("PUT", "/repocheck/v1/webhooks", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Webhooks)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `PUT is not used in this application`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}



// TestWHPOSTEmptyBody tests POST if body is empty -----------------------------
func TestWHPOSTEmptyBody(t *testing.T) {
  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("POST", "/repocheck/v1/webhooks", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Webhooks)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Body is empty. Try:

{
	"event": "(commits|languages|issues|status)",
	"url": "webhook url"
}`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestWHPOST tests POST if body -----------------------------------------------
func TestWHPOST(t *testing.T) {
  // Response getter
  byt := []byte(`{
	"event": "commits",
	"url": "HardangerVidda.com"
}`)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("POST", "/repocheck/v1/webhooks", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Webhooks)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Random genereted key. Won't match`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestWHGETID tests GET if ID is given -----------------------------------------
func TestWHGETID(t *testing.T) {
  // To contact the database
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/webhooks/HZKfzBWK7b8ltuTzZtR3Y1EFJEE3hHToFlRBrl5EJAw", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Webhooks)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `{
    "id": "tu5Wwpa-WqGXrOJ8vOftf0Hgb1Ftgae2a-y4Bl99dqY",
    "event": "commits",
    "url": "HardangerVidda.com",
    "time": "2019-11-03T15:59:02.37027Z"
}`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestWHGETNoID tests GET if ID is not given ----------------------------------
func TestWHGETNoID (t *testing.T) {
  // To contact the database
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/webhooks/", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Webhooks)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `[
    {
        "id": "7YentxQLGu9rVTpqAtNgorOA7O9V9sLc6Ne_UxPqZS0",
        "event": "languages",
        "url": "http://webhook.site/ca9c03d9-2bfb-491a-a8ab-71bf4fd760bb",
        "time": "2019-10-27T12:38:25.247508Z"
    },
    {
        "id": "HZKfzBWK7b8ltuTzZtR3Y1EFJEE3hHToFlRBrl5EJAw",
        "event": "commit",
        "url": "http://webhook.site/ca9c03d9-2bfb-491a-a8ab-71bf4fd760bb",
        "time": "2019-10-27T16:43:31.244078Z"
    },
    {
        "id": "roMXXeXBdF-UDgFrX3AMl9ofWAGhwxX0wFR9Dsnnctw",
        "event": "languages",
        "url": "webhook url",
        "time": "2019-10-27T16:42:38.260863Z"
    },
    {
        "id": "tu5Wwpa-WqGXrOJ8vOftf0Hgb1Ftgae2a-y4Bl99dqY",
        "event": "commits",
        "url": "HardangerVidda.com",
        "time": "2019-11-03T15:59:02.37027Z"
    }
]`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestWHDeleteID tests Delete with ID -----------------------------------------
func TestWHDeleteID(t *testing.T) {
  // To contact the database
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("DELETE", "/repocheck/v1/webhooks/tu5Wwpa-WqGXrOJ8vOftf0Hgb1Ftgae2a-y4Bl99dqY", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Webhooks)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Webhook deleted`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}

// TestWHDeleteNoID tests Delete with ID -----------------------------------------
func TestWHDeleteNoID(t *testing.T) {
  // To contact the database
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("DELETE", "/repocheck/v1/webhooks/", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Webhooks)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `ID does not exist`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}
