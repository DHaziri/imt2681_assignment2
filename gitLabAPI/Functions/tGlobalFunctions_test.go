package Functions

import(
  "net/http"
  "net/http/httptest"
  "testing"
  "bytes"
)


// TestBadRequest tests bad request --------------------------------------------
func TestBadRequest(t *testing.T) {
  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(BadRequest)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Bad Request
  Try insted:

	/repocheck/v1/commits
	/repocheck/v1/commits?limit=[0-9]+
	/repocheck/v1/commits?auth=<access-token>
	/repocheck/v1/commits?limit=[0-9]+&auth=<access-token>

	/repocheck/v1/languages
	/repocheck/v1/languages?limit=[0-9]+
	/repocheck/v1/languages?auth=<access-token>
	/repocheck/v1/languages?limit=[0-9]+&auth=<access-token>

	/repocheck/v1/issues
	/repocheck/v1/issues??type=(users|labels)
	/repocheck/v1/issues?auth=<access-token>
	/repocheck/v1/issues??type=(users|labels)&auth=<access-token>

	/repocheck/v1/status

	/repocheck/v1/webhooks
	/repocheck/v1/webhooks/ID`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}
