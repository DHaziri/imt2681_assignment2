package Functions

import(
  "net/http"
  "net/http/httptest"
  "testing"
  "bytes"
  "gitLabAPI/Variables"
  "log"

  "golang.org/x/net/context"
  "google.golang.org/api/option"

  firebase "firebase.google.com/go"
)


// TestIssuesNoType test issues without type -----------------------------------
func TestIssuesNoType (t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/issues", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Issues)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Bad Request
Try insted:

	/repocheck/v1/commits
	/repocheck/v1/commits?limit=[0-9]+
	/repocheck/v1/commits?auth=<access-token>
	/repocheck/v1/commits?limit=[0-9]+&auth=<access-token>

	/repocheck/v1/languages
	/repocheck/v1/languages?limit=[0-9]+
	/repocheck/v1/languages?auth=<access-token>
	/repocheck/v1/languages?limit=[0-9]+&auth=<access-token>

	/repocheck/v1/issues
	/repocheck/v1/issues??type=(users|labels)
	/repocheck/v1/issues?auth=<access-token>
	/repocheck/v1/issues??type=(users|labels)&auth=<access-token>

	/repocheck/v1/status

	/repocheck/v1/webhooks
	/repocheck/v1/webhooks/ID`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestIssuesWrongType test issues with wrong type -----------------------------
func TestIssuesWrongType (t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/issues?type=huh", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Issues)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Bad Request
Try insted:

	/repocheck/v1/commits
	/repocheck/v1/commits?limit=[0-9]+
	/repocheck/v1/commits?auth=<access-token>
	/repocheck/v1/commits?limit=[0-9]+&auth=<access-token>

	/repocheck/v1/languages
	/repocheck/v1/languages?limit=[0-9]+
	/repocheck/v1/languages?auth=<access-token>
	/repocheck/v1/languages?limit=[0-9]+&auth=<access-token>

	/repocheck/v1/issues
	/repocheck/v1/issues??type=(users|labels)
	/repocheck/v1/issues?auth=<access-token>
	/repocheck/v1/issues??type=(users|labels)&auth=<access-token>

	/repocheck/v1/status

	/repocheck/v1/webhooks
	/repocheck/v1/webhooks/ID`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}



// TestIssuesNoBody test issues with type but no body --------------------------
func TestIssuesNoBody (t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/issues?type=users", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Issues)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Body is empty. Try: {
	"project": "project name"
  }`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestIssuesUsers test issues with type users and auth ------------------------
func TestIssuesUsers (t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(`{
	"project": "onecity"
  }`)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/issues?type=users&auth=6z8oKCNRogYwDMxd9pCy", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Issues)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `{
    "users": [
        {
            "username": "simon.mccallum",
            "count": 1
        },
        {
            "username": "mariusz",
            "count": 18
        }
    ],
    "auth": true
    }`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}


// TestIssuesLabels test issues with type labels and auth ----------------------
func TestIssuesLabels (t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(`{
	"project": "onecity"
  }`)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/issues?type=labels&auth=6z8oKCNRogYwDMxd9pCy", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Issues)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `{
    "lables": [
        {
            "label": "Doing",
            "count": 5
        },
        {
            "label": "Review",
            "count": 8
        }
    ],
    "auth": true
    }`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}
