package Functions

import(
  "net/http"
  "io/ioutil"
  "io"
  "bytes"
  "encoding/json"
  "fmt"
  "strings"
  "time"

  "gitLabAPI/Variables"

  "google.golang.org/api/iterator"
)


// Invocation sends data to Urls that have requested it from certian paths ----
func Invocation (w http.ResponseWriter, event string, para string) {


  // Declaring and filling the object ------------------------------------------
  var invoObject Variables.GetWebInvocation
  invoObject.Event = event
  invoObject.Parameters = para
  invoObject.Time = time.Now()
  invoBytes, err := json.Marshal(invoObject)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }


  // Looks for every webhooks that has asked for data regarding requested input
  iter := Variables.Client.Collection("webHooks").Where("Event", "==", event).Documents(Variables.Ctx)
  for {
    doc, err := iter.Next()
    if err == iterator.Done {
      break
    }


    // Converts struct into bytes ----------------------------------------------
    url := doc.Data()["URL"].(string)
    respons :=  bytes.NewBuffer(invoBytes)
    request, err := http.NewRequest("GET", url, respons)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    //  and gets old of client so it can send data to client
    client := http.Client{}
    _, err = client.Do(request)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
  }
}



// Webhooks register, get or delete a webhook ----------------------------------
func Webhooks(w http.ResponseWriter, r *http.Request){


  // Checks if an ID has been sendt with path ----------------------------------
  ID := ""
  splits := strings.Split(r.URL.Path, "/")
  if len(splits) >= 5 && splits[4] != "" {
    ID = splits[4]
  }


  // If POST, register webhook -------------------------------------------------
  if r.Method == http.MethodPost {

      // Gets data and fills object
      postData, err := ioutil.ReadAll(r.Body)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      var postObject Variables.PostWeb
      err = json.Unmarshal(postData, &postObject)
      if err != nil {
        fmt.Fprintln(w, Variables.EmptyPostBody)

      } else {

        // Registers webhook in database
        wH, _, err := Variables.Client.Collection("webHooks").Add(Variables.Ctx, postObject)
        if err != nil {
          w.WriteHeader(http.StatusInternalServerError)
          return
        }

      fmt.Fprintln(w, wH.ID)
      }


  // If GET, view webhook ------------------------------------------------------
  } else if r.Method == http.MethodGet {

    // If ID was not specified -------------------------------------------------
    if ID == "" {

      // Declares an array of webhooks
      var viewHooks []Variables.ViewWebResponse
      // Goes through every webhook
      iter := Variables.Client.Collection("webHooks").Documents(Variables.Ctx)
      for {
        doc, err := iter.Next()
        if err == iterator.Done {
          break
        }

        // Gets and fills in object
        var getRes Variables.ViewWebResponse
        doc.DataTo(&getRes)
        getRes.ID = doc.Ref.ID
        getRes.Time = doc.CreateTime

        // Add every webhook on to the array of webhooks
        viewHooks = append(viewHooks, getRes)
      }

      // Converts object to bytes and write it out
      viewBytes, err := json.Marshal(viewHooks)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      w.Header().Set("Content-Type", "application/json")
      w.Write(viewBytes)
      w.WriteHeader(http.StatusOK)


    // If ID is specified ------------------------------------------------------
    } else {

      // Gets the webhook with ID
      wH, err := Variables.Client.Collection("webHooks").Doc(ID).Get(Variables.Ctx)
      if err != nil {
        fmt.Fprintln(w, Variables.NoID)
        return
      }

      // Declares and fills the object
      var object Variables.ViewWebResponse
      wH.DataTo(&object)
      object.ID = wH.Ref.ID
      object.Time = wH.CreateTime

      // Converts object to bytes and write it out
      viewBytes, err := json.Marshal(object)
      if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      w.Header().Set("Content-Type", "application/json")
      w.Write(viewBytes)
      w.WriteHeader(http.StatusOK)
    }


  // If DELETE, deletes webhook with specified ID ------------------------------
  } else if r.Method == http.MethodDelete {

    // Gets the webhook with ID
    _, err := Variables.Client.Collection("webHooks").Doc(ID).Delete(Variables.Ctx)
    if err != nil {
      fmt.Fprintln(w, Variables.NoID)
      return
      // As of 27.10.19 kl.21.09 : "Currently, delete does not fail if a document doesn't exist."
    }
    fmt.Fprintln(w, Variables.WHDeleted)


  // If anything else ----------------------------------------------------------
  } else {
      io.WriteString(w, r.Method + Variables.NotInUse)
  }
}
