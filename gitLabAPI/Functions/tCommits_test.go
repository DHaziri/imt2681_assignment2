package Functions

import(
  "net/http"
  "net/http/httptest"
  "testing"
  "bytes"
  "gitLabAPI/Variables"
  "log"

  "golang.org/x/net/context"
  "google.golang.org/api/option"

  firebase "firebase.google.com/go"
)


// TestCommits test commits with both limit and auth ---------------------------
func TestCommit (t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/commits?limit=2&auth=6z8oKCNRogYwDMxd9pCy", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Commits)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `{
  "repos": [
        {
            "path_with_namespace": "research/onecity/onecity",
            "commits": 821
        },
        {
            "path_with_namespace": "msc/BEMoGa/onecity",
            "commits": 281
        }
    ],
    "auth": true
    }`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}



// TestCommitWrongLim test commits with wrong limit ----------------------------
func TestCommitWrongLim(t *testing.T) {
  // To both test Invocation and to make sure the tests doesn't complain
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile("." + Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	defer Variables.Client.Close()


  // Response getter
  byt := []byte(``)

  respons :=  bytes.NewBuffer(byt)

  req, err := http.NewRequest("GET", "/repocheck/v1/commits?limit=h", respons)
  if err != nil {
      t.Fatal(err)
  }

  // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
  rr := httptest.NewRecorder()
  handler := http.HandlerFunc(Commits)

  // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
  // directly and pass in our Request and ResponseRecorder.
  handler.ServeHTTP(rr, req)

  // Check the status code is what we expect.
  if status := rr.Code; status != http.StatusOK {
      t.Errorf("handler returned wrong status code: got %v want %v",
          status, http.StatusOK)
  }

  // Check the response body is what we expect.
  expected := `Limit is not valid (not an int)`
  if rr.Body.String() != expected {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}
