package Functions

import (
    "encoding/json"
    "net/http"
    "time"
    "gitLabAPI/Variables"
)


// Status returns application status -------------------------------------------
func Status(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")


  // Gets the information about the website and database -----------------------
  git, err := http.Get(Variables.GitURLPro)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  database, err := http.Get(Variables.DataBaseURL)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }


  // Declaring and filling the struct ------------------------------------------
  var responseObject Variables.Status
  responseObject.Git = git.StatusCode
  responseObject.Database = database.StatusCode
  responseObject.UpTime = int(time.Since(Variables.StartTime).Seconds())
  responseObject.Version = Variables.Version


  // Converts and writing out the bytes and commits ----------------------------
  statusBytes, err := json.Marshal(responseObject)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  w.Write(statusBytes)
  w.WriteHeader(http.StatusOK)
}
