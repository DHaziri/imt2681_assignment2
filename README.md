# IMT2681_Assignment2

### What is this repository for? ###

An API service that aggregates information from oNTNU's Gitlab deployment  


### How do I get set up? ###

Comming soon to an repository near you


### Who do I talk to? ###

Donjete Haziri
donjete.haziri@ntnu.no

### Why the horrible layout? ###

Becuase go standard is terrible. Commenting looks ugly and forces absolutly unessecary comments, and not beign able to have tests in it's own folder to keep proper track of things are just horrible. This is why there's a t in front of every test file; so it's easier to seperate them from the actual functions

### How to use the app ###

-- Webhooks Post

{
  "event": "languages",  
  "url": "webhook url" 
}




-- Webhooks Delete
As of 27.10.19 kl.21.09 : "Currently, delete does not fail if a document doesn't exist."



-- Lang body

[ "projectName1", "projectName2", ....]



-- Issue body

{
	"project": "project name"
}


### Justification for certian choices ###

- The app only goes through 60 repository id's by if project name is not specified. MaxRepos should be at a much higher number to actually go through all the repositories on the site. As that would take forever to load, I chose to leave it at 60.

- Regardless of if the Access Token is valid or not, Authentication will be put as true
The reason for this is that if someone where to try and access private information it would be bad if they could get proper feedback of how it is supposed to look and if it's valid or not

- There are more than one place where strings are hardcoded rather than used in a constant. Many places in webhooks.go "webHooks" are hardcoded because neither would it save code nor need to be easily changable.
The paths in main.go and w.Header().Set() parameters are also hardcoded as they also wont save code, most likeley wont change and id they where to change, they are easy to find and fix


### Go Testing ###

All tests are failed even if the expected result matched perfectly with the actual result, (maybe with or without the right whitespaces, but I couldn't figure it out). Still, to my human eyes all results are successes.
